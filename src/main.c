#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include "lpc17xx_uart.h"
#include "lpc17xx_pinsel.h"
#include <lpc17xx_gpio.h>
#include <lpc17xx_nvic.h>
#include "lpc17xx_exti.h"
#include <lpc17xx_timer.h>
#include "lpc17xx_adc.h"
#include "stdlib.h"

// Definiciones. 
#define VREF       3.3 //Voltaje de referencia en el pin VREFP, dado VREFN = 0V(GND)
#define ADC_CLK_EN (1<<12)	//
#define SEL_AD0_0  (1<<0) //Seleccionar  Chanal AD0.0
#define CLKDIV     1 //ADC clock-divider (ADC_CLOCK=PCLK/CLKDIV+1) = 12.5Mhz @ 25Mhz PCLK
#define PWRUP      (1<<21) //Setear a 0 => power down
#define START_CNV  (1<<24) //001 para iniciar la conversión inmediatamente
#define ADC_DONE   (1U<<31) //define it as unsigned value or compiler will throw #61-D warning
#define ADCR_SETUP_SCM ((CLKDIV<<8) | PWRUP) //

// Funciones de Configuración
void pinConfig(void);
void uartConfig(void);
void TMR0_Config(void);
void ADC_Config(void);

// Funciones de rutina de interrupción
void RutinaIntUART(void);
void RutinaIntTMR(void);

// Funciones que comandan GPIO
void MostrarModo(void);
void Bomba(void);

// Funciones para imprimir mensajes
void MenuGral(void);
void MenuModo(void);
void SolicitarDataModo1(void);
void ErrorModo(void);
void ErrorHab(void);
void Limpiar(void);

// Variables globales
uint8_t modo = 0;
uint8_t NivelTanque = 50;
uint8_t SetNivelPC = 0;
uint8_t SetNivelADC = 0;
uint8_t EstadoBomba = 2;
uint8_t HabControl = 0;	// 0 -> Deshabilitado; 1 -> Habilitado

uint8_t recibir1[4];

int a = 0;

void EINT3_IRQHandler(void)
{
	NVIC_ClearPendingIRQ(EINT3_IRQn);
	if(GPIO_GetIntStatus(0,9,0)==ENABLE)
	{
		if(modo == 0)
			EstadoBomba = 1;	// Vaciando el tanque
		GPIO_ClearInt(0,(1<<9));	// Bajar bandera por P0.9
	}
	if(GPIO_GetIntStatus(0,8,0)==ENABLE)
	{
		if(modo == 0)
			EstadoBomba = 2;	// Detenida
		GPIO_ClearInt(0,(1<<8));	// Bajar bandera por P0.8
	}
	if(GPIO_GetIntStatus(0,7,0)==ENABLE)
	{
		if(modo == 0)
			EstadoBomba = 3;	// Llenando el tanque
		GPIO_ClearInt(0,(1<<7));	// Bajar bandera por P0.7
	}
	EXTI_ClearEXTIFlag(EXTI_EINT3);	// Bajar bandera de interrupción externa 3
}

void UART0_IRQHandler(void)
{
	SetNivelADC /= 2.55;
	RutinaIntUART();
	NVIC_ClearPendingIRQ(UART0_IRQn);
}

void TIMER0_IRQHandler()
{
	SetNivelADC /= 2.55;
	a = ~a;
	RutinaIntTMR();
	NVIC_ClearPendingIRQ(TIMER0_IRQn);
	LPC_TIM0->IR |= (1<<0);
	TIM_ClearIntPending(LPC_TIM0, TIM_MR0_INT);
}


int main(void)
{
	uint16_t valor = 0;

	// Configurar
	pinConfig();
	uartConfig();
	TMR0_Config();
	ADC_Config();

	// Imprimir mensaje inicial
    MenuGral();

	while(1)
    {
		MostrarModo();
    	Bomba();
    	LPC_ADC->ADCR |= START_CNV; //Start new Conversion
    	while((LPC_ADC->ADDR0 & ADC_DONE) == 0); //Wait untill conversion is finished
    	valor = ADC_GetData(0); // Obtener datos del canal 0.
    	SetNivelADC = (uint8_t)(valor >> 4);
    }
    return 0 ;
}

void Limpiar(void)
{
	uint8_t info0[1] = {27};

	UART_Send(LPC_UART0,info0,sizeof(info0),BLOCKING);

	return;
}

void MenuGral(void)
{
	uint8_t info0[]  = "\nModo actual: ";
	uint8_t info1[1] = {modo + 48};
	uint8_t info2[]  = "\nNivel de tanque seteado por PC: "; // Por algún motivo cuando es de una
	uint8_t info3[3];										 // sola cifra le mete un caracter
	uint8_t info4[] =	"\nNivel actual del tanque: ";
	uint8_t info5[3];
	uint8_t info6[]  = "\nNivel de tanque seteado por ADC: ";
	uint8_t info7[3];
	uint8_t info8[]  = "\nIngrese el valor adecuado para seleccionar entre las siguientes opciones\n"
			"    0: Deshabilitar Auto-Control\n    1: Habilitar Auto-Control\n    2: Cambiar nivel"
			" establecido por PC\n    3: Cambiar modo de funcionamiento\n";
	uint8_t Hab[] = "\nAuto-Control Habilitado";
	uint8_t Deshab[] = "\nAuto-Control Deshabilitado";

	itoa(SetNivelPC,info3,10);	// Convertir el valor "SetNivel" a ASCII en sistema decimal
	itoa(NivelTanque,info5,10); // Idem NivelTanque
	itoa(SetNivelADC,info7,10);	// Idem SetNivelADC

	if(HabControl)
		UART_Send(LPC_UART0,Hab,sizeof(Hab),BLOCKING);
	else
		UART_Send(LPC_UART0,Deshab,sizeof(Deshab),BLOCKING);
	UART_Send(LPC_UART0,info0,sizeof(info0),BLOCKING);
	UART_Send(LPC_UART0,info1,sizeof(info1),BLOCKING);
	UART_Send(LPC_UART0,info2,sizeof(info2),BLOCKING);
	UART_Send(LPC_UART0,info3,sizeof(info3),BLOCKING);
	UART_Send(LPC_UART0,info4,sizeof(info4),BLOCKING);
	UART_Send(LPC_UART0,info5,sizeof(info5),BLOCKING);
	UART_Send(LPC_UART0,info6,sizeof(info6),BLOCKING);
	UART_Send(LPC_UART0,info7,sizeof(info7),BLOCKING);
	UART_Send(LPC_UART0,info8,sizeof(info8),BLOCKING);

	return;
}

void MenuModo(void)
{
	uint8_t info[] = "\nSeleccione el modo en que desea operar:\n    0: Modo Manual\n"
			"    1: Control de nivel por PC\n    2: Control de nivel por pote\n"
			"    3: Modo Limpieza\n";

	UART_Send(LPC_UART0,info,sizeof(info),BLOCKING);

	return;
}

void SolicitarDataModo1(void)
{
	uint8_t info[] = "\nModo 1 seleccionado. Ingrese el nivel al que desea mantener el tanque"
			"\n0000 -> 0% \n0100 -> 100% \n";

		UART_Send(LPC_UART0,info,sizeof(info),BLOCKING);
		UART_Send(LPC_UART0,"\n",sizeof("\n"),BLOCKING);

		return;
}

void ErrorModo(void)
{
	uint8_t info[] = "\nError, ese modo ya se encuentra seleccionado. Ingrese un modo distinto al actual\n";

	UART_Send(LPC_UART0,info,sizeof(info),BLOCKING);

	return;
}

void ErrorHab(void)
{
	uint8_t info[] = "\nError: Auto-Control debe estar deshabilitado para ejecutar ese comando\n";

	UART_Send(LPC_UART0,info,sizeof(info),BLOCKING);

	return;
}

void MostrarModo(void)
{
	switch(modo)
	{
		case 0:
			GPIO_SetValue(0,(1<<0));
			GPIO_ClearValue(0,(1<<1));
			GPIO_ClearValue(0,(1<<18));
			GPIO_ClearValue(0,(1<<17));
			break;
		case 1:
			GPIO_ClearValue(0,(1<<0));
			GPIO_SetValue(0,(1<<1));
			GPIO_ClearValue(0,(1<<18));
			GPIO_ClearValue(0,(1<<17));
			break;
		case 2:
			GPIO_ClearValue(0,(1<<0));
			GPIO_ClearValue(0,(1<<1));
			GPIO_SetValue(0,(1<<18));
			GPIO_ClearValue(0,(1<<17));
			break;
		case 3:
			GPIO_ClearValue(0,(1<<0));
			GPIO_ClearValue(0,(1<<1));
			GPIO_ClearValue(0,(1<<18));
			GPIO_SetValue(0,(1<<17));
			break;
		default:
			GPIO_ClearValue(0,(1<<0));
			GPIO_ClearValue(0,(1<<1));
			GPIO_ClearValue(0,(1<<18));
			GPIO_ClearValue(0,(1<<17));
	}
	return;
}

void Bomba(void)
{
	switch(EstadoBomba)
		{
			case 1:
				GPIO_SetValue(2,(1<<0));
				GPIO_ClearValue(2,(1<<1));
				GPIO_ClearValue(2,(1<<2));
				break;
			case 2:
				GPIO_ClearValue(2,(1<<0));
				GPIO_SetValue(2,(1<<1));
				GPIO_ClearValue(2,(1<<2));
				break;
			case 3:
				GPIO_ClearValue(2,(1<<0));
				GPIO_ClearValue(2,(1<<1));
				GPIO_SetValue(2,(1<<2));
				break;

			default:
				GPIO_ClearValue(2,(1<<0));
				GPIO_SetValue(2,(1<<1));
				GPIO_ClearValue(2,(1<<2));
		}
		return;
}

void RutinaIntUART(void)
{
	static uint8_t opc_sel = 5;	// Opción seleccionada del menu gral
	uint8_t recibir0[1];
	uint32_t intsrc, tmp, tmp1;
	UART_FIFO_CFG_Type UARTFIFO_cfg;

	// Ver cual es la fuente de interrupción
	intsrc = UART_GetIntId(LPC_UART0);
	tmp = intsrc & UART_IIR_INTID_MASK;

	// Si se reciven datos:
	if((tmp == UART_IIR_INTID_RDA) || (tmp == UART_IIR_INTID_CTI))
	{
		if(opc_sel == 5)
		{
			UART_Receive(LPC_UART0, recibir0, sizeof(recibir0), NONE_BLOCKING);
			opc_sel = atoi(recibir0);
			if(opc_sel == 0)	// Deshabilitar Bomba
			{
				HabControl = 0;
				EstadoBomba = 2;
				opc_sel = 5;
				MenuGral();
			}
			if(opc_sel == 1)	// Habilitar Bomba
			{
				HabControl = 1;
				opc_sel = 5;
				MenuGral();
			}
			if(opc_sel == 2)	// Cambiar Nivel Establecido por PC
			{
				if(!HabControl)
				{
					SolicitarDataModo1();
					UART_FIFOConfigStructInit(&UARTFIFO_cfg);
					UARTFIFO_cfg.FIFO_Level = UART_FIFO_TRGLEV1;
					UART_FIFOConfig(LPC_UART0, &UARTFIFO_cfg);
					// Ahora se recibirán 4 caracteres
				}
				else
				{
					ErrorHab();
					opc_sel = 5;
					MenuGral();
				}
			}
			if(opc_sel == 3)	// Seleccionar Modo
			{
				if(!HabControl)
					MenuModo();
				else
				{
					ErrorHab();
					opc_sel = 5;
					MenuGral();
				}
			}
		}
		else
		{
			if(opc_sel == 2) // Dato para setear el nivel.
			{
				UART_Receive(LPC_UART0, recibir1, sizeof(recibir1), NONE_BLOCKING);
				SetNivelPC = atoi(recibir1);
				UART_FIFOConfigStructInit(&UARTFIFO_cfg);
				UARTFIFO_cfg.FIFO_Level = UART_FIFO_TRGLEV0;
				UART_FIFOConfig(LPC_UART0, &UARTFIFO_cfg);
				// Ahora se recibirá un solo caracter
				opc_sel = 5;
				MenuGral();
			}
			if(opc_sel == 3) // Dato para seleccionar modo
			{
				UART_Receive(LPC_UART0, recibir0, sizeof(recibir0), NONE_BLOCKING);
				if(atoi(recibir0) != modo)
				{
					modo = atoi(recibir0);
				}
				else
				{
					ErrorModo();
				}
				opc_sel = 5;
				MenuGral();
			}
		}
	}

	// Si hubo alg{un error
	//UART_IIR_INTID_RLS -> Interrupt identification: Receive line status
	if(tmp == UART_IIR_INTID_RLS)
	{
		tmp1 = UART_GetLineStatus(LPC_UART0);
		tmp1 &= (UART_LSR_OE | UART_LSR_PE | UART_LSR_FE | UART_LSR_BI | UART_LSR_RXFE);
		if(tmp1)	// Si ubo alguno de esos errores, bucle infinito. Aqui se podría intentar
		{			// solucionar el error
			while(1){GPIO_SetValue(0,(1<<15));}	// Encender P0.15 indicando error y solicitando reset
		}
	}
	return;
}

void RutinaIntTMR(void)
{
	if(modo == 0)
		{
			if(EstadoBomba == 1)
			{
				if(NivelTanque == 0)
				{
					EstadoBomba = 2;
				}
				else
				{
					NivelTanque--;
					MenuGral();
				}
			}
			if(EstadoBomba == 3)
			{
				if(NivelTanque == 100)
				{
					EstadoBomba = 2;
				}
				else
				{
					NivelTanque++;
					MenuGral();
				}
			}
		}
		if(HabControl)
		{
			if(modo == 1)
			{
				if(NivelTanque > SetNivelPC)
				{
					EstadoBomba = 1;

						NivelTanque--;
						MenuGral();
				}
				if(NivelTanque < SetNivelPC)
				{
					EstadoBomba = 3;
					NivelTanque++;
					MenuGral();
				}
				if(NivelTanque == SetNivelPC)
				{
					EstadoBomba = 2;
					modo = 0;
					HabControl = 0;
					MenuGral();
				}
			}
			if(modo == 2)
			{
				if(NivelTanque > SetNivelADC)
				{
					EstadoBomba = 1;

						NivelTanque--;
						MenuGral();
				}
				if(NivelTanque < SetNivelADC)
				{
					EstadoBomba = 3;
					NivelTanque++;
					MenuGral();
				}
				if(NivelTanque == SetNivelADC)
				{
					EstadoBomba = 2;
					modo = 0;
					HabControl = 0;
					MenuGral();
				}
			}
			if(modo == 3)
			{
				FIO_SetMask(0, 0x00000380, 0);	// Enmascarar pines P0.7, P0.8 y P0.9
				if(NivelTanque == 0)
				{
					EstadoBomba = 2;
					modo = 0;
					HabControl = 0;
					FIO_SetMask(0, 0x00000380, 1);	// Desenmascarar pines P0.7, P0.8 y P0.9
				}
				else
				{
					EstadoBomba = 1;
					NivelTanque--;
				}
				MenuGral();
			}
		}
		return;
}

void pinConfig(void)
{
	PINSEL_CFG_Type pinCFG;
	pinCFG.Portnum = 0;
	pinCFG.Pinnum = 2;
	pinCFG.Funcnum = 1;
	pinCFG.Pinmode = 0;
	pinCFG.OpenDrain = 0; 		// Sin opendrain
	PINSEL_ConfigPin(&pinCFG);
	GPIO_SetDir(0,(1<<2),1);	// P0.2 es una salida

	pinCFG.Pinnum=3;
	PINSEL_ConfigPin(&pinCFG);	// Configurar p0.3 como UART0 Rx
	GPIO_SetDir(0,(1<<3),0);	// P0.3 es una entrada

	//-------------------------------------------------

	pinCFG.Portnum=0;
	pinCFG.Funcnum=0;

	pinCFG.Pinnum=0;
	PINSEL_ConfigPin(&pinCFG);

	pinCFG.Pinnum=1;
	PINSEL_ConfigPin(&pinCFG);

	pinCFG.Pinnum=18;
	PINSEL_ConfigPin(&pinCFG);

	pinCFG.Pinnum=17;
	PINSEL_ConfigPin(&pinCFG);

	pinCFG.Pinnum=15;			// Este es para prender un led si hay un error de uart
	PINSEL_ConfigPin(&pinCFG);

	// Pines para interrupciones externas
	pinCFG.Pinmode = PINSEL_PINMODE_PULLDOWN;

	pinCFG.Pinnum=7;
	PINSEL_ConfigPin(&pinCFG);

	pinCFG.Pinnum=8;
	PINSEL_ConfigPin(&pinCFG);

	pinCFG.Pinnum=9;
	PINSEL_ConfigPin(&pinCFG);

	//---- Configurar P0.0, P0.1, P0.18, P0.17 y P0.15 como GPIO ----

	GPIO_SetDir(0,0x00068003,1);	// Configurar P0.0, P0.1, P0.18, P0.17 y P0.15 como salidas

	GPIO_SetDir(0,0x00000380,0);	// Configurar P0.9, P0.8 y P0.7 como entradas

	GPIO_IntCmd(0,0x00000380,0); // 0-> Flanco Ascendente
	NVIC_EnableIRQ(EINT3_IRQn);

	pinCFG.Portnum=2;

	pinCFG.Pinnum=0;
	PINSEL_ConfigPin(&pinCFG);

	pinCFG.Pinnum=1;
	PINSEL_ConfigPin(&pinCFG);

	pinCFG.Pinnum=2;
	PINSEL_ConfigPin(&pinCFG);

	//---- Configurar P2.0, P2.1, P2.2 como GPIO ----

	GPIO_SetDir(2,0x00000007,1);	// Configurar P2.0, P2.1, P2.2 como salidas

	// Pin para ADC:

	pinCFG.Portnum = 0; 	// Seleccionar Puerto 0
	pinCFG.Pinnum = 23;	 	// Seleccionar Pin 23
	pinCFG.Funcnum = 1; 	// P0 Pin0 funciona como AD0[0]

	PINSEL_ConfigPin (&pinCFG); // Efectuar la configuración.

	GPIO_SetDir(0,(1<<23),0);	// P0.23 Entrada

	return;
}

void TMR0_Config()
{
	TIM_TIMERCFG_Type tmr;
	TIM_MATCHCFG_Type mtch;

	tmr.PrescaleOption = TIM_PRESCALE_USVAL;
	tmr.PrescaleValue = 100;
	mtch.MatchChannel = 0;
	mtch.IntOnMatch = ENABLE;
	mtch.StopOnMatch = DISABLE;
	mtch.ResetOnMatch = ENABLE;
	mtch.ExtMatchOutputType = TIM_EXTMATCH_NOTHING;
	mtch.MatchValue = 10000;

	TIM_Init(LPC_TIM0,TIM_TIMER_MODE,&tmr);
	TIM_ConfigMatch(LPC_TIM0,&mtch);

	TIM_Cmd(LPC_TIM0, ENABLE);
	NVIC_EnableIRQ(TIMER0_IRQn);

	return;
}

void ADC_Config(void)
{
	LPC_SC->PCONP |= ADC_CLK_EN; //Enable ADC clock
	LPC_ADC->ADCR =  ADCR_SETUP_SCM | SEL_AD0_0;
	LPC_PINCON->PINSEL1 |= (1<<14) ; //select AD0.0 for P0.23

	return;
}

void uartConfig(void)
{
	UART_CFG_Type UART_cfg;
	UART_FIFO_CFG_Type UARTFIFO_cfg;

	// UART_cfg.Baud_rate = 9600;
	// UART_cfg.Databits = UART_DATABIT_8;
	// UART_cfg.Parity = UART_Parity_NONE;
	// UART_cfg.Stopbits = UART_STOPBIT_1;
	// En vez de hacer esto se usa la siguiente función que configura UART_cfg
	UART_ConfigStructInit(&UART_cfg);	// con los valores comentados.

	// Esta funci{on inicializa el periférico y lo configura con los valores del struct
	UART_Init(LPC_UART0, &UART_cfg);

    //UARTFIFO_cfgt->FIFO_DMAMode = DISABLE;	// Utilización o no de DMA
	//UARTFIFO_cfg->FIFO_Level = UART_FIFO_TRGLEV0;	// Se dispara una interrpción
	//o se genera un DMA request cuando se reciben uno, dos, cuatro, ocho o 14 caracteres.
	//UARTFIFO_cfg->FIFO_ResetRxBuf = ENABLE;
	//UARTFIFO_cfg->FIFO_ResetTxBuf = ENABLE;
	// En vez de hacer esto se hace idem UART_ConfigStructInit(&UART_cfg);
	UART_FIFOConfigStructInit(&UARTFIFO_cfg);
	//UARTFIFO_cfg.FIFO_Level = UART_FIFO_TRGLEV1;

	// Aplicar configuración UART_FIFO en LPC_UARTX (en este caso UART0)
	UART_FIFOConfig(LPC_UART0, &UARTFIFO_cfg);

	// Habilita la transmisión
	UART_TxCmd(LPC_UART0, ENABLE);

	// Habilita interrupción por Rx en UART0
	UART_IntConfig(LPC_UART0, UART_INTCFG_RBR, ENABLE);

	// Habilita interrupción por estado de la linea Rx (por si hay algun tipo de error)
	UART_IntConfig(LPC_UART0, UART_INTCFG_RLS, ENABLE);

	// Habilita interrupción por pefiférico UART (cada un caracter por la config de arriba)
	NVIC_EnableIRQ(UART0_IRQn);

	return;
}