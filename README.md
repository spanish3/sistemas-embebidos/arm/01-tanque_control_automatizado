# 01-Tanque_Control_Automatizado

Este repositorio contiene el código fuente para un Sistema de Control Automatizado de Tanques desarrollado utilizando un microcontrolador LPC con bibliotecas CMSIS en MCU Expresso.
Placa: LPC1769

## Descripción

El Sistema de Control Automatizado de Tanques está diseñado para gestionar eficientemente los niveles de líquido en un tanque a través de varios modos de entrada. Ofrece una interfaz fácil de usar para configuración y monitoreo, utilizando comunicación UART para interacción con el usuario.

![Diagrama](img/diagram.png)

## Componentes y Funcionalidades

### Sensores
- Utiliza el ADC para medir el voltaje del potenciómetro para el Control basado en Potenciómetro (P0.23).
- Monitorea pines de interrupción externa para llenar el tanque (P0.7), detenerse (P0.8) o vaciar (P0.9).

### Actuadores
- Controla la bomba o el sistema de válvulas para el flujo de líquido dentro/fuera del tanque.
- Gestiona indicadores LED para indicación de estado del sistema y señalización de errores.
  - El LED P0.0 se enciende en el modo 0.
  - El LED P0.1 se enciende en el modo 1.
  - El LED P0.18 se enciende en el modo 2.
  - El LED P0.17 se enciende en el modo 3.
  - El LED P0.15 se enciende con error de UART.

### Microcontrolador y Periféricos
- Utiliza un microcontrolador LPC, configurando pines GPIO y periféricos de temporizador (TMR0) para tareas específicas.
- Se comunica con UART para la interacción con dispositivos externos, permitiendo entrada de usuario, configuración y visualización de estado.

### Modos de Control
- Soporta múltiples modos de funcionamiento:
  - 0: Modo Manual: Permite el control manual del llenado y vaciado del tanque.
  - 1: Control a través de PC: Permite configurar el nivel del tanque (%) mediante comandos UART desde una PC conectada.
  - 2: Control basado en Potenciómetro: Posiblemente permite el control usando un potenciómetro para establecer el nivel de agua deseado del tanque (%).
  - 3: Modo de Limpieza: Un modo dedicado para operaciones de limpieza del tanque.

### Lógica de Control
- El algoritmo de control monitorea continuamente el sensor de nivel del tanque, dictando operaciones de la bomba basadas en modos seleccionados y puntos de ajuste predefinidos.
- Los modos de control automático ajustan la operación de la bomba para mantener el nivel de tanque deseado establecido por la PC u otros métodos.

### Interacción con el Usuario
- Proporciona una interfaz basada en menús a través de UART para que los usuarios seleccionen modos operativos, establezcan niveles de tanque y habiliten/deshabiliten el control automatizado.

### Manejo y Monitoreo de Errores
- Implementa manejo de errores para errores de comunicación UART utilizando indicadores LED y banderas del sistema.
- Monitorea el estado de la línea de comunicación UART en busca de posibles errores e incorpora mecanismos para señalar y manejar estos errores.

## Licencia

Este proyecto está bajo la [Licencia MIT](LICENSE.md).
